//import './App.scss';
import "./App.sass";
import "./styles.css";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
// import { Splide, SplideSlide } from '@splidejs/react-splide';

import Slider from "react-slick";
import { useState } from "react";
import axios from "axios";

function App() {
  const sliderSettings = {
    dots: false,
    infinite: true,
    autoplay: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <button>sdsds</button>
  };

  function handleClass() {
    if (response !== undefined)
      if (response.length === 0) return "bg-grey";
      else return "bg-green";
    else return "bg-yellow";
  }

  function handleClick(i) {
    // console.log(i);
    const question = messages[i];
    setSelectedQuestion(question);
    // console.log(selectedQuestion);
  }

  function handleChange(event) {
    setSelectedQuestion(event.target.value);
  }

  function ShowMessages(messages) {
    const messageList = messages.map((message, index) => (
      <div className="input" key={index} onClick={() => handleClick(index)}>
        <p>{message}</p>
      </div>
    ));
    return messageList;
  }

  async function getResponse() {
    //const params = new URLSearchParams();
    //params.append('text', 'Hola!')
    const data = {
      text: JSON.stringify(selectedQuestion)
    };
    const headers = {
      "Content-Type": "application/json"
    };
    return await axios
      .post("https://webhook.wholemeaning.com/webhook/tool-demo", data, {
        headers
      })
      .then(({ data }) => {
        // Handle success
        console.log(data.template);
        setResponse(data.template);
      });
  }

  const messages = [
    "Hola, no he recibido el número de seguimiento. ¿Donde está mi pedido?",
    "Hola, necesito cambiar el destino de un paquete, me equivoqué al escribir la dirección",
    "Hola, ¿Cual es mi codigo postal?",
    "Hola, ¿Cual es el horario de la sucursal?"
  ];

  const [selectedQuestion, setSelectedQuestion] = useState("");
  const [response, setResponse] = useState("");

  return (
    <div className="App">
      <header className="App-header">
        <div style={{ width: "600px", background: "", alignItems: "center" }}>
          <div
            style={{
              width: "600px",
              background: "white",
              "border-radius": "20px",
              alignItems: "center",
              padding: "20px 15px 20px 15px",
              "box-sizing": "border-box"
            }}
          >
            <div>
              <Slider {...sliderSettings}>{ShowMessages(messages)}</Slider>
            </div>
          </div>
        </div>

        <div className="holder2">
          <div className="holder">
            <textarea
              value={selectedQuestion}
              onChange={handleChange}
              placeholder={"Escriba su requerimiento acá..."}
            />
          </div>
          <button className="button-test" onClick={getResponse}>
            >
          </button>
        </div>

        <div className="holder3" style={{ color: "black" }}>
          <div className={handleClass()}>
            <span className="test">
              {response === ""
                ? "Aún no se ha seleccionado un requerimiento"
                : response === undefined
                ? "Ooops! No se ha encontrado una respuesta para el requerimiento. Su caso será derivado a un ejecutivo para que lo resuelva."
                : response}
            </span>
          </div>
        </div>
      </header>
    </div>
  );
}

export default App;
